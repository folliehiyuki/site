import sitemap from "lume/plugins/sitemap.ts";
import icons, { Options as IconsOptions } from "lume/plugins/icons.ts";
import basePath from "lume/plugins/base_path.ts";
import sourceMaps from "lume/plugins/source_maps.ts";
import redirects from "lume/plugins/redirects.ts";
import sass from "lume/plugins/sass.ts";
import date from "lume/plugins/date.ts";
import minifyHTML from "lume/plugins/minify_html.ts";
import sri, { Options as SriOptions } from "lume/plugins/sri.ts";
import feed, { Options as FeedOptions } from "lume/plugins/feed.ts";
import unocss, { Options as UnoCSSOptions } from "lume/plugins/unocss.ts";
import toc from "lume_markdown_plugins/toc/mod.ts";
import footnotes from "lume_markdown_plugins/footnotes/mod.ts";
import markdownItShiki from "shikijs_markdown-it";
import markdownItGitHubAlerts from "markdown-it-github-alerts";
import {
  transformerNotationDiff,
  transformerNotationErrorLevel,
  transformerNotationFocus,
  transformerNotationHighlight,
  transformerNotationWordHighlight,
  transformerStyleToClass,
} from "shikijs_transformers";

import { Page } from "lume/core/file.ts";
import "lume/types.ts";

interface Options {
  icons?: Partial<IconsOptions>;
  feed?: Partial<FeedOptions>;
  sri?: Partial<SriOptions>;
  unocss?: Partial<UnoCSSOptions>;
}

// TODO: explicitly set icon variants here
const options: Options = {
  feed: {
    output: ["/feed.xml", "/feed.json"],
    query: "type=post",
    info: {
      title: "=metas.site",
      description: "=metas.description",
    },
  },
  sri: {
    selector: [
      "script[src]",
      "link[rel=stylesheet][href]",
      "link[rel=preload][href]",
      "link[rel=modulepreload][href]",
    ].join(),
  },
  unocss: {
    cssFile: "styles/unocss.css",
  },
};

const styleToClass = transformerStyleToClass({
  classPrefix: "__shiki_",
});

// deno-lint-ignore no-explicit-any
const markdownItPlugins: Array<any> = [
  toc,
  footnotes,
  markdownItGitHubAlerts,
  await markdownItShiki({
    themes: {
      light: "catppuccin-latte",
      dark: "catppuccin-macchiato",
    },
    transformers: [
      transformerNotationDiff(),
      transformerNotationErrorLevel(),
      transformerNotationFocus(),
      transformerNotationHighlight(),
      transformerNotationWordHighlight(),
      styleToClass,
    ],
  }),
];

export default function () {
  return (site: Lume.Site) => {
    markdownItPlugins.forEach((plugin) =>
      Array.isArray(plugin)
        ? site.hooks.addMarkdownItPlugin(...plugin)
        : site.hooks.addMarkdownItPlugin(plugin)
    );

    // Add the generated Shiki CSS style to the output directory
    site.process([".css"], function (_, allPages) {
      const pageMap = Page.create({
        url: "/styles/shiki.css",
        content: styleToClass.getCSS(),
      });

      allPages.push(pageMap);
    });

    site.use(icons(options.icons))
      .use(sri())
      .use(basePath())
      .use(date())
      .use(sourceMaps())
      .use(redirects())
      .use(sass())
      .use(unocss(options.unocss))
      .use(minifyHTML())
      .use(sitemap())
      .use(feed(options.feed));

    site.copy("_static", ".");
  };
}
