let content = "";

try {
  const fileResponse = await fetch(
    "https://github.com/ai-robots-txt/ai.robots.txt/raw/main/robots.txt",
  );

  if (fileResponse.body) {
    content = await fileResponse.text();
  }
} catch (e) {
  throw e;
}

export default function () {
  return {
    path: "/_static/robots.txt",
    content: content,
  };
}
